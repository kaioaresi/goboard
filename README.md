# Goboard k8s

Objetivos:

1. Criar um desenho para exemplificar o que será feito.



```mermaid
sequenceDiagram
    participant HC as HealthCheck
    participant CD as CheckDB
    participant CC as cadastraCluster
    participant BC as buscaCluster
    participant UC as updateCluster
    participant DC as deleteCluster
    participant DB as DB

    CD->>DB: Ping
    DB-->>CD: Pong
    CD-->>HC: DB OK
    CC->>DB: Registra cluster
    BC->>DB: Consulta cluster
    UC->>DB: Atualiza Cluster
    DC->>DB: Deleta Cluster
```

## Implementação

**Framework http**

Name | Stars | Forks | Open issues
:---:|:---:|:---:|:---:
[gin](https://github.com/gin-gonic/gin#gin-web-framework) | 40.2k | 4.6k | 238
[beego](https://github.com/astaxie/beego) | 24.5k | 4.9k | 746
[echo](https://github.com/labstack/echo) | 17.7k | 1.6k | 37

Framework a ser utilizado `echo`

```
go get github.com/labstack/echo/v4
```

**Lib banco de dados**

Drive sql: `github.com/go-sql-driver/mysql`

```
go get github.com/go-sql-driver/mysql
```

**Lib logs**

Framework logs:

- [Zap](https://pkg.go.dev/go.uber.org/zap)

```
go get -u go.uber.org/zap
```


- [Zerolog](https://github.com/rs/zerolog)
- [Logrus](https://github.com/sirupsen/logrus)

**Distributed Tracing**

Framework tracing and APM

- [OpenTelemetry](https://opentelemetry.io/)
- [Signoz](https://signoz.io/)

**Layout diretorios**

```
.
├── Dockerfile
├── README.md
├── build
├── cmd
│   └── goboard
│       └── goboard.go
├── docs
├── go.mod
├── go.sum
├── infra
│   ├── README.md
│   └── db.yaml
├── internal
│   ├── db
│   └── router
│       ├── healthcheck.go
│       └── healthcheck_test.go
├── pkg
└── test
```


---

# Referencias

- [Framworks http](https://www.geeksforgeeks.org/top-5-golang-frameworks-in-2020/)
- [gin](https://github.com/gin-gonic/gin#gin-web-framework)
- [beego](https://github.com/astaxie/beego)
- [echo](https://github.com/labstack/echo)
- [Framworks logs](https://blog.logrocket.com/5-structured-logging-packages-for-go/)
- [Zap](https://pkg.go.dev/go.uber.org/zap)
- [Zerolog](https://github.com/rs/zerolog)
- [Golang diretorio estandard](https://github.com/golang-standards/project-layout/blob/master/README_ptBR.md)
- [socorro como posso organizar os pacotes em go km0](https://dev.to/odilonjk/socorro-como-posso-organizar-os-pacotes-em-go-km0)
- [SQLInterface](https://github.com/golang/go/wiki/SQLInterface)
- [Zap log](https://medium.com/codex/level-based-logging-in-go-with-uber-zap-a8a90aa40672)
- [manage connections](https://cloud.google.com/sql/docs/mysql/manage-connections#go)
