package main

import (
	"goboard/configs"
	"goboard/internal/router"

	"github.com/labstack/echo"
)

func init() {
	configs.GetConfig()
}

func main() {

	e := echo.New()
	e.GET("/health", router.HealthCheck)
	e.Logger.Fatal(e.Start(":8080"))

}
