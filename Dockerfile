## Build app
FROM golang:1.18-alpine as builder

WORKDIR /app

COPY  . .

RUN go mod download

RUN go build cmd/goboard/goboard.go

## App context
FROM alpine:3.16.2

WORKDIR /app

RUN apk update \
    && apk -U upgrade \
    && apk add --no-cache ca-certificates bash gcc \
    && update-ca-certificates --fresh \
    && rm -f /var/cache/apk/*

RUN addgroup app && adduser -S app -u 1000 -G app

COPY --chown=app:app --from=builder /app/goboard .

RUN chmod +x /app/goboard

USER app

EXPOSE 8080

CMD ["/app/goboard"]
