# Configs db

```
// Criando database goboard
CREATE DATABASE goboard;

// Criando user da app
CREATE USER 'goboard'@'%' IDENTIFIED BY 'g0b0Ard';

Select user from mysql.user;

// Add permissões para database goboard acesso remoto
GRANT ALL PRIVILEGES ON goboard.* TO 'goboard'@'%';

FLUSH PRIVILEGES;
```


```
CREATE DATABASE goboard;
CREATE USER 'goboard'@'%' IDENTIFIED BY 'g0b0Ard';
Select user from mysql.user;
GRANT ALL PRIVILEGES ON goboard.* TO 'goboard'@'%';
FLUSH PRIVILEGES;
```

```
show variables like 'max_connections';
```




```
// Deleta usuário
DROP USER 'goboard'@'%';
```