package configs

import (
	"fmt"
	"goboard/internal/logger"
	"os"
)

type DB struct {
	Host     string
	User     string
	Pass     string
	Database string
	Port     string `default:"3306"`
}

type Config struct {
	ConfigDB DB
}

// GetConfig - configura envs
func GetConfig(params ...string) Config {

	// var config Config

	config := Config{ConfigDB: DB{
		Host:     os.Getenv("DB_HOST"),
		User:     os.Getenv("DB_USER"),
		Pass:     os.Getenv("DB_PASS"),
		Database: os.Getenv("DB_DATABASE"),
		Port:     os.Getenv("DB_PORT"),
	}}

	if err := validateConfigEnv(config); err != nil {
		logger.Logger().Sugar().Fatal(err)
	}

	return config
}

// validateConfigEnv - realiza validação das envs
func validateConfigEnv(c Config) error {

	if c.ConfigDB.Host == "" {
		return fmt.Errorf("env Host DB não informada")
	}
	if c.ConfigDB.User == "" {
		return fmt.Errorf("env User DB não informada")
	}
	if c.ConfigDB.Pass == "" {
		return fmt.Errorf("env Pass DB não informada")
	}
	if c.ConfigDB.Database == "" {
		return fmt.Errorf("env Database DB não informada")
	}

	return nil
}
