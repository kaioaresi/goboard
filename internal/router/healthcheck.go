package router

import (
	"goboard/internal/db"
	"net/http"

	"github.com/labstack/echo"
	"go.uber.org/zap"
)

// Health - Struct para facilitar retorno de informações para healthcheck
type Health struct {
	Service string `json:"service"`
	Status  string `json:"status"`
}

// HealthCheck - verificar se a conexão com services estão ok
func HealthCheck(c echo.Context) error {
	logger, _ := zap.NewDevelopment()
	defer logger.Sync()

	banco, _ := db.ConnectionDB()

	if err := db.Ping(banco); err != nil {
		logger.Sugar().Errorf("Erro de conexão com banco\n%v\n", err)
		return err
	}

	statusDB := Health{"mysql", "ok"}
	return c.JSON(http.StatusOK, &statusDB)
}
