package db

import (
	"database/sql"
	"fmt"
	"goboard/configs"
	"goboard/internal/logger"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB = nil

func ConnectionDB() (*sql.DB, error) {
	if db == nil {

		// vars DB Config
		dbconfig := configs.GetConfig()

		connectionString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=True&loc=Local", dbconfig.ConfigDB.User, dbconfig.ConfigDB.Pass, dbconfig.ConfigDB.Host, dbconfig.ConfigDB.Port, dbconfig.ConfigDB.Database)

		d, err := sql.Open("mysql", connectionString)

		if err != nil {
			logger.Logger().Sugar().Fatal("Erro ao se conexão com banco de dados\n%v\n", err)
			return nil, err
		}

		configPoll(d)
		db = d
	}

	return db, nil
}
