package db

import (
	"database/sql"
	"fmt"
	"goboard/internal/logger"
)

// Ping - valida conexão com banco
func Ping(db *sql.DB) error {

	db, err := ConnectionDB()
	if err != nil {
		logger.Logger().Sugar().Fatalf("Erro conexão com banco\n%v\n", err)
		return err
	}

	if err = db.Ping(); err != nil {
		logger.Logger().Sugar().Fatalf("Erro ping com banco\n%v\n", err)
		return err
	}

	fmt.Println("Conectado ao banco de dados!")
	return nil
}
