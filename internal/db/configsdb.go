package db

import (
	"database/sql"
	"time"
)

// configPoll - Config poll conexão db
func configPoll(db *sql.DB) {
	//// Config poll conexões
	// Numero max de conexões em IDLE
	db.SetMaxIdleConns(5)
	// Numero máximo de conexões abertas
	db.SetMaxOpenConns(5)
	// Timeout para conexões IDLE
	db.SetConnMaxIdleTime(1 * time.Second)
	// TTL conexões
	db.SetConnMaxLifetime(30 * time.Second)

}
