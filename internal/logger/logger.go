package logger

import "go.uber.org/zap"

var logger *zap.Logger = nil

// Logger - Func de logs
func Logger() *zap.Logger {
	if logger == nil {
		l, _ := zap.NewDevelopment()
		// defer logger.Sync()

		logger = l
	}
	return logger
}
